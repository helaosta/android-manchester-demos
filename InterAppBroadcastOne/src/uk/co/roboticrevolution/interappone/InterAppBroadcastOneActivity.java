package uk.co.roboticrevolution.interappone;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class InterAppBroadcastOneActivity extends Activity
{
	private static final String	ACTION_REGISTER_SUCCESS	= "uk.co.roboticrevolution.ACTION_REGISTER_SUCCESS";
	private static final String	ACTION_REGISTER_FAIL	= "uk.co.roboticrevolution.ACTION_REGISTER_FAIL";
	private static final String	ACTION_REGISTER_CHECK	= "uk.co.roboticrevolution.ACTION_REGISTER_CHECK";
	
	private static final String PERMISSION_REGISTER_CHECK = "uk.co.roboticrevolution.CHECK_LICENSE";

	BroadcastReceiver receiver;
	TextView box;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		box = (TextView)findViewById(R.id.boxView);

		if (true)
		{
			receiver = new BroadcastReceiver()
			{

				@Override
				public void onReceive(Context context, Intent intent) 
				{
					if (intent.getAction().contentEquals(ACTION_REGISTER_SUCCESS))
					{
						box.post(new Runnable()
						{
							
							@Override
							public void run() 
							{
								box.setBackgroundResource(R.color.Green);
							}
						});
						
						Toast.makeText(InterAppBroadcastOneActivity.this, "Registered", Toast.LENGTH_LONG).show();
					}
					else
						if (intent.getAction().contentEquals(ACTION_REGISTER_FAIL))
						{
							box.post(new Runnable()
							{
								
								@Override
								public void run() 
								{
									box.setBackgroundResource(R.color.Red);
								}
							});
							
							Toast.makeText(InterAppBroadcastOneActivity.this, "Not Registered", Toast.LENGTH_LONG).show();
						}

				}
			};

			
			// Runtime registration of Intent filter for a broadcast receiver.
			IntentFilter filter = new IntentFilter();
			filter.addAction(ACTION_REGISTER_SUCCESS);
			filter.addAction(ACTION_REGISTER_FAIL);

			registerReceiver(receiver, filter);
			
		}

	}
	
	@Override
	protected void onResume() 
	{
		super.onResume();
		
		Intent i = new Intent(ACTION_REGISTER_CHECK);
		i.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
		sendBroadcast(i, PERMISSION_REGISTER_CHECK);
		
		Log.i("INTERAPP", "App One - Broadcasting Intent");
		
	}
	
	@Override
	protected void onDestroy() 
	{
		unregisterReceiver(receiver);
		
		super.onDestroy();
	}
}