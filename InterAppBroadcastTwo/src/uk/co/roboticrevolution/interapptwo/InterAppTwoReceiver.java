package uk.co.roboticrevolution.interapptwo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class InterAppTwoReceiver extends BroadcastReceiver
{
	private static final String	ACTION_REGISTER_SUCCESS	= "uk.co.roboticrevolution.ACTION_REGISTER_SUCCESS";
	private static final String	ACTION_REGISTER_FAIL	= "uk.co.roboticrevolution.ACTION_REGISTER_FAIL";
	private static final String	ACTION_REGISTER_CHECK	= "uk.co.roboticrevolution.ACTION_REGISTER_CHECK";
	

	@Override
	public void onReceive(Context context, Intent intent) 
	{
		Log.e("INTERAPP", "App Two - Intent Received");
		
		if(intent.getAction().contentEquals(ACTION_REGISTER_CHECK))
		{	
			checkRegistrationState(context);
		}
	}
	
	private void checkRegistrationState(Context context)
	{
		Intent i;
		
		if(isRegistered())
		{
			i = new Intent(ACTION_REGISTER_SUCCESS);
		}
		else
		{
			i = new Intent(ACTION_REGISTER_FAIL);
		}
		
		context.sendBroadcast(i);
	}
	
	private boolean isRegistered()
	{
		// DO some licensing validation here or work out if we are registered and legitimate.
		
		return true;
//	    return false;
	}

}
