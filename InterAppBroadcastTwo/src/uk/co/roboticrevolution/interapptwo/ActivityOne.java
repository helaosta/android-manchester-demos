package uk.co.roboticrevolution.interapptwo;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class ActivityOne extends Activity
{
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		Log.e("TWO", "App Two Started");
		super.onCreate(savedInstanceState);
	}
}
