package uk.co.roboticrevolution.secretcodedemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class MyReceiver extends BroadcastReceiver
{
	private static final String ACTION_SECRET_CODE = "android.provider.Telephony.SECRET_CODE";
	

	@Override
	public void onReceive(Context c, Intent i) 
	{
		if(i.getAction().contentEquals(ACTION_SECRET_CODE))
		{
			Intent newIntent = new Intent(c, SecretCodeActivity.class);
			newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			c.startActivity(newIntent);
		}
	}
	
}