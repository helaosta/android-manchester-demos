package uk.co.roboticrevolution.secretcodedemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

public class SecretCodeActivity extends Activity 
{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secret_code);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_secret_code, menu);
        return true;
    }
}
