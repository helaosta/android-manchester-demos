package uk.co.roboticrevolution.parcelextra;

import uk.co.roboticrevolution.parcelextra.objects.MyParcelableObject;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ParcelExtraDemoReceiver extends BroadcastReceiver
{
	MyParcelableObject obj;

	@Override
	public void onReceive(Context context, Intent intent) 
	{
		String action = intent.getAction();
		
		if(action.contentEquals(ParcelExtraDemoActivity.ACTION_SEND_PARCEL))
		{
			obj = intent.getExtras().getParcelable(ParcelExtraDemoActivity.EXTRA_MYPARCEL);
		}
		
	}
}
