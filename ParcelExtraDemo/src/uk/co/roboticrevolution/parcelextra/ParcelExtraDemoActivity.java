package uk.co.roboticrevolution.parcelextra;

import java.util.ArrayList;

import uk.co.roboticrevolution.parcelextra.objects.MyParcelableObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class ParcelExtraDemoActivity extends Activity
{
	public static final String	ACTION_SEND_PARCEL	= "uk.co.roboticrevolution.ACTION_SEND_PARCEL";
	public static final String	EXTRA_MYPARCEL		= "myparcel";

	@Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        MyParcelableObject obj = new MyParcelableObject();
        
        obj.setMyString("Test String");
        obj.setMyInt(2012);
        obj.setMyDouble(1.7976931348623157E308d);
        
        ArrayList<String> newList = new ArrayList<String>();
        newList.add(new String("First String"));
        newList.add(new String("Second String"));
        newList.add(new String("Third String"));
        newList.add(new String("Fourth String"));
        newList.add(new String("Firth String"));
        obj.setMyArrayList(newList);
        
        Intent i = new Intent(ACTION_SEND_PARCEL);
        i.putExtra(EXTRA_MYPARCEL, obj);
        sendBroadcast(i);
    }

}