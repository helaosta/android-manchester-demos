package uk.co.roboticrevolution.parcelextra.objects;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class MyParcelableObject implements Parcelable
{
	private String myString;
	private int myInt;
	private double myDouble;
	private ArrayList<String> myArrayList;
	
	// Default constructor
	public MyParcelableObject()
	{
		myArrayList = new ArrayList<String>();
	}
	
	// Parcel constructor
	public MyParcelableObject(Parcel in)
	{
		this.myString = in.readString();
		this.myInt = in.readInt();
		this.myDouble = in.readDouble();
		
		myArrayList = new ArrayList<String>();
		in.readList(myArrayList, String.class.getClassLoader());
	}
	
	
	// Typical getters and setters
	
	public void setMyString(String string)
	{
		this.myString = string;
	}
	
	public String getMyString()
	{
		return this.myString;
	}
	
	public void setMyInt(int val)
	{
		this.myInt = val;
	}
	
	public int getMyInt()
	{
		return this.myInt;
	}
	
	public void setMyDouble(double val)
	{
		this.myDouble = val;
	}
	
	public double getMyDouble()
	{
		return this.myDouble;
	}
	
	public void setMyArrayList(ArrayList<String> list)
	{
		this.myArrayList = list;
	}
	
	public ArrayList<String> getMyArrayList()
	{
		return this.myArrayList;
	}
	
	// Important code for Parcel 

	@Override
	public int describeContents() 
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) 
	{
		dest.writeString(myString);
		dest.writeInt(myInt);
		dest.writeDouble(myDouble);
		dest.writeList(myArrayList);
	}
	
	// This static call is essential for the OS to be able to handle your object.
	
	public static final Parcelable.Creator<MyParcelableObject> CREATOR = new Parcelable.Creator<MyParcelableObject>()
	{
		public MyParcelableObject createFromParcel(Parcel in)
		{
			return new MyParcelableObject(in);
		}
		
		public MyParcelableObject[] newArray(int size)
		{
			return new MyParcelableObject[size];
		}
	};

}
