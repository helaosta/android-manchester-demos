package uk.co.roboticrevolution.intentextras;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends Activity
{
	public static final String EXTRA_TEXT	=	"extra_text";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_two);
		setTitle("Activity Two");
		
		Bundle extras = getIntent().getExtras();
		
		if(extras != null)
		{
			String text = extras.getString(EXTRA_TEXT);
			
			TextView textView = (TextView)findViewById(R.id.textBox);
			textView.setText(text);
		}
		
	}
}
