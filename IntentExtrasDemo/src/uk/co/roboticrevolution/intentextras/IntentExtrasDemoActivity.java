package uk.co.roboticrevolution.intentextras;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class IntentExtrasDemoActivity extends Activity implements OnClickListener 
{
	EditText editBox;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setTitle("Activity One");
        
        editBox = (EditText)findViewById(R.id.inputBox);
        
        Button go = (Button)findViewById(R.id.goButton);
        go.setOnClickListener(this);
    }

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
			case R.id.goButton:
				sendDataToActivityTwo();
			break;
			default:
				// do nothing
		}
		
	}
	
	private void sendDataToActivityTwo()
	{
		Intent i = new Intent(IntentExtrasDemoActivity.this, SecondActivity.class);
		i.putExtra(SecondActivity.EXTRA_TEXT, editBox.getText().toString());
		
//		i.putExtra(EXTRA_BOOLEAN, true);
//		i.putExtra(EXTRA_INT, 100);
//		...
		
		startActivity(i);
	}
}